#ifndef MY_JSON_UTILS_H
#define MY_JSON_UTILS_H

#include <Arduino.h>
#include <ArduinoJson.h>

template <typename T>
static void arrayToJson(const T array[], size_t size, JsonVariant json) {
  json.to<JsonArray>();
  for (size_t i = 0; i < size; i++) {
    if (!json.add((uint8_t) array[i])) {
      Serial.println(F("{\"error\": \"JSON doc too small!\"}"));
    }
  }
}

template <typename T>
int arrayMerge(T dest[], size_t size, JsonVariantConst src) {
  JsonObjectConst obj = src.as<JsonObjectConst>();
  if (obj) {
    for (JsonPairConst kvp : obj)
    {
      unsigned index = atoi(kvp.key().c_str());
      if (index < size) {
        dest[index] = kvp.value().as<T>();
      }
    }
    return 0;
  }
  
  JsonArrayConst arr = src.as<JsonArrayConst>();
  if (arr) {
    for (size_t i = 0; i < arr.size() && i < size; i++) {
      dest[i] = arr[i].as<T>();
    }
    return 0;
  }

  // Other types not supported
  return -1;
}

#endif
