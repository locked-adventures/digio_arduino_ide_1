#include <Arduino.h>
#include <HardwareSerial.h>
#include <SPI.h>
#include <ArduinoJson.h>

#include "ShiftRegisterDuo.hpp"
#include "my_json_utils.hpp"

#define SERIAL_SPEED 115200

#define SPI_SPEED 1000000

#define IN  F("in")
#define OUT F("out")
#define INIT F("init")

#define HC595_CLEAR         (2)
#define HC595_STORAGE_CLOCK (3)
#define HC595_OUTPUT_ENABLE (4)
#define HC165_SHIFT         (5)

#define N_PINS (16)

#define LOOP_DELAY 5
#define INPUT_STABLE_CYCLES 5

#define ACK F("ack")
#define REQUEST_ID F("reqid")

#define ERROR F("error")
#define TYPE F("type")
#define CODE F("code")
#define DESCRIPTION F("description")

#define JSON_DOC_SIZE 300

bool out[N_PINS];
bool in[N_PINS];

int inCounter[N_PINS];

ShiftRegisterDuo shiftRegisterDuo(
  HC165_SHIFT,
  HC595_CLEAR,
  HC595_STORAGE_CLOCK,
  HC595_OUTPUT_ENABLE,
  SPI_SPEED,
  N_PINS);

constexpr size_t LINE_SIZE = 128;
char line[LINE_SIZE];
size_t line_len = 0;

bool inNew[N_PINS];

StaticJsonDocument<JSON_DOC_SIZE> clientJson;
StaticJsonDocument<JSON_DOC_SIZE> outputJson;

void (*resetFunc) (void) = 0;

void setup() {
  Serial.begin(SERIAL_SPEED);
  while (!Serial);
  shiftRegisterDuo.init();
  shiftRegisterDuo.transfer(in, out);
  shiftRegisterDuo.setOutputEnabled(true);

  outputJson[INIT] = true;
  outputJson[IN].to<JsonArray>();
  arrayToJson(in, N_PINS, outputJson[IN]);
  outputJson[OUT].to<JsonArray>();
  arrayToJson(out, N_PINS, outputJson[OUT]);

  Serial.println(F("\r\nBEGIN"));

  serializeJson(outputJson, Serial);
  Serial.println();
  outputJson.clear();
}

// Handle client input line
void handleLine(const char line[]) {
  outputJson.clear();
  auto &ackJson = outputJson;
  DeserializationError err = deserializeJson(clientJson, line);
  ackJson[ACK].to<JsonObject>();
  if (err == DeserializationError::Ok) {
    JsonObject obj = clientJson.as<JsonObject>();
    if (obj) {
      ackJson[ACK][REQUEST_ID] = clientJson[REQUEST_ID];
      JsonVariant clientOutput = clientJson[OUT];
      if (!clientOutput.isNull()) {
        if (arrayMerge(out, N_PINS, clientJson[OUT])) {
          ackJson[ACK][ERROR][TYPE] = F("content");
          ackJson[ACK][ERROR][DESCRIPTION] = F("invalid output request");
        }
        else {
          ackJson[ACK][OUT].to<JsonArray>();
          for (auto o : out) {
            if (!ackJson[ACK][OUT].add((uint8_t) o)) {
              Serial.println(F("{\"error\": \"JSON doc too small!\"}"));
            }
          }
          // Print output array for debugging
          /* Serial.print("out: [");
          bool first = true;
          for (auto o : out) {
            if (!first)
              Serial.print(", ");
            Serial.print(o);
            first = false;
          }
          Serial.println("]"); */
        }
      }
    }
    else {
      ackJson[ACK][ERROR][TYPE] = F("content");
      ackJson[ACK][ERROR][DESCRIPTION] = F("request should be an object");
    }
  }
  else {
    ackJson[ACK][ERROR][TYPE] = F("deserialization");
    ackJson[ACK][ERROR][CODE] = err.c_str();
  }
  // ackJson["testkey"] = "testval";
  serializeJson(ackJson, Serial);
  Serial.println();
  ackJson.clear();
}

bool line_overflow = false;
bool buffer_overflow = false;

void loop() {
  if (!buffer_overflow && Serial.available() > SERIAL_RX_BUFFER_SIZE - 16) {
    buffer_overflow = true;
    resetFunc();
  }
  if (!line_overflow) {
    int c = Serial.read();
    if (c != -1) {
      char ch = c;
      if (ch == '\0') {
        Serial.println(F("{\"error\": \"Input char cannot be \'\\0\'!\"}"));
      }
      else {
        line[line_len] = ch;
        line_len++;
        line[line_len] = '\0';
        if (ch == '\n') {
          handleLine(line);
          line_len = 0;
          line[line_len] = '\0';
        }
        else if (line_len >= LINE_SIZE - 1) {
          Serial.println(F("{\"error\": \"Line is becoming too large!\"}"));
          line_overflow = true;
          resetFunc();
        }
      }
    }
  }

  shiftRegisterDuo.transfer(inNew, out);

  auto &eventJson = outputJson;
  eventJson.clear();
  for (size_t i = 0; i < N_PINS; i++) {
    if (inNew[i] == in[i]) {
      // Reset de-bouncing counter
      inCounter[i] = 0;
    }
    else {
      // De-bouncing
      inCounter[i]++;
      if (inCounter[i] >= INPUT_STABLE_CYCLES) {
        in[i] = inNew[i];

        // eventJson[IN].to<JsonObject>();

        char key[4];
        // No official way to print size_t, so we have to convert to long
        if (snprintf(key, sizeof(key), "%lu", (unsigned long) i) < 0) {
          Serial.println(F("{\"error\": \"snprintf failed!\"}"));
        }
        eventJson[IN][key] = (uint8_t) in[i];

        // Reset de-bouncing counter
        inCounter[i] = 0;
      }
    }
  }

  if (!eventJson.isNull()) {
    serializeJson(eventJson, Serial);
    Serial.println();
  }

  eventJson.clear();

  Serial.flush();

  delay(LOOP_DELAY);
}
