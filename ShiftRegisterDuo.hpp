#ifndef SHIFT_REGISTER_DUO_HPP
#define SHIFT_REGISTER_DUO_HPP

#include <stdint.h>
class ShiftRegisterDuo {
    int hc165_shift;  // low: load; high: shift
    int hc595_clear;  // low-active
    int hc595_storageClock;  // positive edge triggered
    int hc595_outputEnable;  // low-active
    uint32_t spiSpeed;
    int nPins;  // number of input and output pins each

public:
    // Empty constructor, keeps object uninitialized
    ShiftRegisterDuo() {}

    // Constructor
    ShiftRegisterDuo(int hc165_shift, int hc595_clear, int hc595_storageClock, int hc595_outputEnable, uint32_t spiSpeed, int nPins)
        : hc165_shift(hc165_shift)
        , hc595_clear(hc595_clear)
        , hc595_storageClock(hc595_storageClock)
        , hc595_outputEnable(hc595_outputEnable)
        , spiSpeed(spiSpeed)
        , nPins(nPins)
    {}

    // Initialize hardware
    void init();

    /**
     * HC595: Clear the shift register.
     */
    void clearOutput();

    /**
     * HC595: transfer shift register state to storage register for output.
     */
    void storeOutput();

    /**
     * HC595: If enabled the values of the storage register are output,
     * otherwise the outputs are high-Z.
     */
    void setOutputEnabled(bool enabled);

    /**
     * Both arrays should have nPins elements
     * Write out values to output shift register
     * If in is not NULL, the values from the input shift register are read into this array
     * 
     * The first element is for the pin nearest to the Arduino
     * The last element is for the last pin of the shift register
     */
    void transfer(bool in[], const bool out[]);
};

#endif
